package com.nagarro.javafreshertraining.assignment4;

import com.nagarro.javafreshertraining.assignment4.linkedlist.LinkedListImplementation;
import com.nagarro.javafreshertraining.assignment4.stack.StackImplementation;
import com.nagarro.javafreshertraining.assignment4.util.Constants;
import com.nagarro.javafreshertraining.assignment4.queue.QueueImplementation;
import com.nagarro.javafreshertraining.assignment4.priorityqueue.PriorityQueueImplementation;
import com.nagarro.javafreshertraining.assignment4.nchildtree.NChildTreeImplementation;
import com.nagarro.javafreshertraining.assignment4.hashtable.HashTableImplementation;

import java.util.Scanner;

/**
 * @author nikitajain
 *
 */
public class Main {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		@SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
		String choice = null;
		while (choice!="EXIT") {
			printInfo();
			choice = scanner.nextLine();
			switch (choice) {
			case "LL":
				new LinkedListImplementation().linkedListImplementation();
				break;
			case "S":
				new StackImplementation().stackImplementation();
				break;
			case "Q":
				new QueueImplementation().queueImplementation();
				break;
			case "PQ":
				new PriorityQueueImplementation().priorityQueueImplementation();
				break;
			case "nT":
				new NChildTreeImplementation().nChildTreeImplementation();
				break;
			case "HT":
				new HashTableImplementation().hashTableImplementation();
				break;
			case "EXIT":
			    System.out.println(Constants.EXIT);
				System.exit(0);
				break;
			default:
				System.out.println("Please enter a valid choice");
			}

		}

	}

	private static void printInfo() {
		System.out.println("**************Main Menu**************");
		System.out.println("Choose a DATA STRUCTURE from list ...");
		System.out.println("LL  - for Linked List");
		System.out.println("S   - for Stack");
		System.out.println("Q   - for Queue");
		System.out.println("PQ  - for Priority Queue");
		System.out.println("nT  - for n-ary Tree");
		System.out.println("HT  - for Hash Table");
		System.out.println("EXIT- for exiting the program");
		System.out.println("Enter code of DS of your choice now-");
	}
}