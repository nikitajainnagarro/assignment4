package com.nagarro.javafreshertraining.assignment4.hashtable;

import java.util.ArrayList;
import java.util.Iterator;

import com.nagarro.javafreshertraining.assignment4.util.Constants;

@SuppressWarnings("rawtypes")
public class HashTableIterator implements Iterator {
	private int curr = 0;
	private int size = 0;
	HashTableNode head = null;
	private int i = 0;
	String data = null;
	String key = null;
	ArrayList<HashTableNode> bucketArray = null;

	/**
	 * @param ht
	 * iterating over the entire hash table
	 */
	public HashTableIterator(HashTableImplementation ht) {
		this.size = ht.size;
		this.head = ht.bucketArray.get(this.curr);
		this.bucketArray = ht.bucketArray;
	}

	public boolean hasNext() {
		return this.i != this.size;
	}

	public String next() {
		while (this.head == null) {

			this.curr = this.curr + 1;
			this.head = bucketArray.get(curr);
		}
		this.data = this.head.value;
		this.key = this.head.key;
		this.head = this.head.next;

		this.i++;
		String keyValue =Constants.KEY + key + "\n" +Constants.VALUE + data;
		return keyValue;
	}

}
