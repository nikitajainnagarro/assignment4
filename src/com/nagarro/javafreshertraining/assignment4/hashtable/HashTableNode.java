package com.nagarro.javafreshertraining.assignment4.hashtable;

//A node of chains
class HashTableNode {
	String key;
	String value;

	// Reference to next node
	HashTableNode next;

	/**
	 * @param key
	 * @param value
	 */
	public HashTableNode(String key, String value) {
		this.key = key;
		this.value = value;
	}
}
