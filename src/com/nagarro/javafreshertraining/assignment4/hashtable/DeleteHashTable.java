package com.nagarro.javafreshertraining.assignment4.hashtable;

import com.nagarro.javafreshertraining.assignment4.exception.UserDefinedException;
import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class DeleteHashTable {
	private static final String DELETED = "Key value deleted";

	// Method to remove a given key
	/**
	 * @param key
	 * @param hashTable 
	 * function deletes the key value pair from the hash table
	 * based on the given key
	 */
	public void deleteByKey(String key, HashTableImplementation hashTable) {
		try {
			if (hashTable.size == 0)
				throw new UserDefinedException(Constants.EMPTYHASHTABLE);
			else {
				// Apply hash function to find index for given key
				int bucketIndex = hashTable.getBucketIndex(key, hashTable);

				// Get head of chain
				HashTableNode head = hashTable.bucketArray.get(bucketIndex);
				// Search for key in its chain
				HashTableNode prev = null;
				while (head != null) {
					// If Key found
					if (head.key.equals(key))
						break;

					// Else keep moving in chain
					prev = head;
					head = head.next;
				}
				// If key was not there
				if (head == null)
					throw new UserDefinedException(Constants.NOTPRESENT);

				// Reduce size
				hashTable.size--;
				// Remove key
				if (prev != null)
					prev.next = head.next;

				else
					hashTable.bucketArray.set(bucketIndex, head.next);
				System.out.println(DELETED);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
