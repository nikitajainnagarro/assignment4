package com.nagarro.javafreshertraining.assignment4.hashtable;

import com.nagarro.javafreshertraining.assignment4.exception.UserDefinedException;
import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class ReadHashTable {
	private static final String VALUE_FOR_KEY = "The value for the key is";

	/**
	 * @param hashTable
	 * function prints all the key value pairs present in the hash table
	 */
	public void printTable(HashTableImplementation hashTable) {
		try {
			// if the hash table is empty
			if (hashTable.size == 0)
				throw new UserDefinedException(Constants.EMPTYHASHTABLE);
			else {

				for (int i = 0; i < hashTable.numBuckets; i++) {
					if (hashTable.bucketArray.get(i) != null) {
						HashTableNode temp = hashTable.bucketArray.get(i);
						while (temp != null) {
							System.out.println(Constants.KEY + temp.key);
							System.out.println(Constants.VALUE + temp.value);
							temp = temp.next;
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @param key
	 * @param hashTable
	 * function finds the value based on the given key
	 */

	public void getValueByKey(String key, HashTableImplementation hashTable) {
		// Find head of chain for given key
		try {

			// if the hash table is empty
			if (hashTable.size == 0)
				throw new UserDefinedException(Constants.EMPTYHASHTABLE);
			else {
				int flag = 0;
				int bucketIndex = hashTable.getBucketIndex(key, hashTable);
				HashTableNode head = hashTable.bucketArray.get(bucketIndex);

				// Search key in chain
				while (head != null) {
					if (head.key.equals(key))
						System.out.println(VALUE_FOR_KEY + head.value);
					head = head.next;
					flag = 1;
				}

				if (flag == 0)
					throw new UserDefinedException(Constants.NOTPRESENT);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
