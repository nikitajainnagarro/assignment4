package com.nagarro.javafreshertraining.assignment4.hashtable;

import java.util.ArrayList;

class CreateHashTable {

	/**
	 * @param hashTable
	 * @return function returns the size of the hash table
	 */
	public int size(HashTableImplementation hashTable) {
		return hashTable.size;
	}

	// Returns value for a key

	// Adds a key value pair to hash
	/**
	 * @param key
	 * @param value
	 * @param hashTable function inserts new value to the hash table
	 */
	public void insert(String key, String value, HashTableImplementation hashTable) {
		// Find head of chain for given key
		int bucketIndex = hashTable.getBucketIndex(key, hashTable);
		HashTableNode head = hashTable.bucketArray.get(bucketIndex);
		// Check if key is already present
		while (head != null) {
			if (head.key.equals(key)) {
				head.value = value;
				return;
			}

			head = head.next;
		}
		// Insert key in chain
		hashTable.size++;
		head = hashTable.bucketArray.get(bucketIndex);
		HashTableNode newNode = new HashTableNode(key, value);
		newNode.next = head;
		hashTable.bucketArray.set(bucketIndex, newNode);
		// If load factor goes beyond threshold, then
		// double hash table size
		if ((1.0 * hashTable.size) / hashTable.numBuckets >= 0.7) {
			ArrayList<HashTableNode> temp = hashTable.bucketArray;
			hashTable.bucketArray = new ArrayList<>();
			hashTable.numBuckets = 2 * hashTable.numBuckets;
			hashTable.size = 0;
			for (int iterator = 0; iterator < hashTable.numBuckets; iterator++)
				hashTable.bucketArray.add(null);

			for (HashTableNode headNode : temp) {
				while (headNode != null) {
					insert(headNode.key, headNode.value, hashTable);
					headNode = headNode.next;
				}
			}
		}
	}
}
