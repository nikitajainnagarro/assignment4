package com.nagarro.javafreshertraining.assignment4.hashtable;

import java.util.ArrayList;
import java.util.Scanner;

import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class HashTableImplementation {
	private static final String ELEMENTS_HASHTABLE = "Elements of table are:";
	private static final String ELEMENTS_COUNT = "No. of elements in hashtable are: ";
	private static final String ENTERVALUE_TO_INSERT = "Enter value to insert";
	private static final String ENTERKEY_TO_INSERT = "Enter key to insert";
	private static final String ENTERKEY_TO_SEARCH = "Enter key you want to search for";
	private static final String ENTERKEY_TO_DELETE = "Enter key you want to delete";
	private static final String ENTERSIZE = "Enter the size of hash table you want to create";
	private static final String GETVALUE = "4.Get value by key";
	Scanner scanner = new Scanner(System.in);
	// bucketArray is used to store array of chains
	ArrayList<HashTableNode> bucketArray;
	protected String key;
	// Current capacity of array list
	protected int numBuckets;
	protected int size;

	public ArrayList<HashTableNode> getBucketArray() {
		return bucketArray;
	}

	public void setBucketArray() {
		this.bucketArray = new ArrayList<>();
		this.numBuckets = scanner.nextInt();
		// Create empty chains
		for (int i = 0; i < numBuckets; i++)
			bucketArray.add(null);
	}

	// Constructor (Initializes capacity, size and
	// empty chains.
	public HashTableImplementation() {

		// Create empty chains
		for (int i = 0; i < numBuckets; i++)
			bucketArray.add(null);
	}

	// This implements hash function to find index
	// for a key
	protected int getBucketIndex(String key, HashTableImplementation hashTable) {
		int hashCode = key.hashCode();
		int index = hashCode % hashTable.numBuckets;
		// key.hashCode() could be negative.
		index = index < 0 ? index * -1 : index;
		return index;
	}

	/**
	 * @param
	 * function is used to play with all the functionalities of the hash table
	 */
	public void hashTableImplementation() {

		HashTableImplementation hashTable = new HashTableImplementation();
		System.out.println(ENTERSIZE);

		hashTable.setBucketArray();
		CreateHashTable create = new CreateHashTable();
		DeleteHashTable delete = new DeleteHashTable();
		ReadHashTable read = new ReadHashTable();
		HashTableContains contains = new HashTableContains();
		String choice = null;
		while (choice != "9") {

			printInfo();
			choice = scanner.nextLine();
			switch (choice) {
			case "1":
				System.out.println(ENTERKEY_TO_INSERT);

				key = scanner.next();
				System.out.println(ENTERVALUE_TO_INSERT);
				String value = scanner.next();
				create.insert(key, value, hashTable);
				break;
			case "2":
				System.out.println(ENTERKEY_TO_DELETE);
				key = scanner.next();

				delete.deleteByKey(key, hashTable);
				break;
			case "3":
				System.out.println(ENTERKEY_TO_SEARCH);
				key = scanner.next();
				contains.isContains(key, hashTable);
				break;
			case "4":
				System.out.println(ENTERKEY_TO_SEARCH);
				key = scanner.next();

				read.getValueByKey(key, hashTable);
				break;
			case "5":
				System.out.println(ELEMENTS_COUNT + create.size(hashTable));
				break;
			case "6":
				System.out.println(ELEMENTS_HASHTABLE);
				HashTableIterator hashTableIterator = new HashTableIterator(hashTable);
				while (hashTableIterator.hasNext()) {
					System.out.println(hashTableIterator.next());
				}
				break;
			case "7":
				read.printTable(hashTable);
				break;
			case "8":
				return;
			case "9":
			    System.out.println("EXIT");
				System.exit(0);
				break;
			default:
				System.out.println(Constants.DEFAULT);
			}

		}
	}

	private void printInfo() {
		System.out.println("Choose one of the following Hash Table operations-");
		System.out.println(Constants.INSERT);
		System.out.println(Constants.DELETE);
		System.out.println(Constants.THREE+Constants.CONTAINS);
		System.out.println(GETVALUE);
		System.out.println(Constants.FIVE+Constants.SIZE);
		System.out.println(Constants.SIX+Constants.ITERATOR);
		System.out.println(Constants.SEVEN+Constants.TRAVERSE);
		System.out.println(Constants.EIGHT+Constants.RETURN);
		System.out.println(Constants.NINE+"EXIT");
		System.out.println(Constants.CHOICE);
	}

}
