package com.nagarro.javafreshertraining.assignment4.hashtable;

import com.nagarro.javafreshertraining.assignment4.exception.UserDefinedException;
import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class HashTableContains {
	/**
	 * @param key
	 * @param hashTable
	 * function checks weather the hash table contains the given key or not
	 */
	public void isContains(String key, HashTableImplementation hashTable) {
		// Find head of chain for given key
		try {
			if (hashTable.size == 0)
				throw new UserDefinedException(Constants.EMPTYHASHTABLE);
			else {
				int flag = 0;
				int bucketIndex = hashTable.getBucketIndex(key, hashTable);
				HashTableNode head = hashTable.bucketArray.get(bucketIndex);

				// Search key in chain
				while (head != null) {
					if (head.key.equals(key))
						System.out.println(Constants.KEYPRESENT);
					head = head.next;
					flag = 1;
				}

				if (flag == 0)
					throw new UserDefinedException(Constants.NOTPRESENT);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
