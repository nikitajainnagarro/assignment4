package com.nagarro.javafreshertraining.assignment4.stack;

import com.nagarro.javafreshertraining.assignment4.exception.UserDefinedException;
import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class ReadStack {

	private static final String STACKSIZE = "Size of stack is: ";
	private static final String STACKELEMENTS = "Elements of stack are:";

	/**
	 * @param stack the function is used to print/traverse the entire stack
	 */
	public void printStack(StackImplementation stack) {
		try {
			// if the stack is empty
			if (stack.head == null) {
				throw new UserDefinedException(Constants.EMPTYSTACK);
			} else {
				StackNode currNode = stack.head;

				System.out.println(STACKELEMENTS);
				while (currNode != null) {
					// Print the data at current node
					System.out.println(currNode.data + " ");
					// Go to next node
					currNode = currNode.next;

				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @param stack this function prints the top of the stack
	 */
	public void peek(StackImplementation stack) {
		try {

			// if the stack is empty
			if (stack.head == null) {
				throw new UserDefinedException(Constants.EMPTYSTACK);
			} else {
				System.out.println(Constants.PEEKIS + stack.head.data);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @param stack this function prints the size of the stack
	 */
	public void sizeOfStack(StackImplementation stack) {
		System.out.println(STACKSIZE + stack.stackSize);
	}

	/**
	 * @param stack this function is used to reverse the entire stack
	 */
	public void reverse(StackImplementation stack) {
		try {

			// if the stack is empty
			if (stack.head == null) {
				throw new UserDefinedException(Constants.EMPTYSTACK);
			} else {
				StackNode current = stack.head;
				StackNode prev = null;
				StackNode next = null;
				while (current != null) {
					next = current.next;
					current.next = prev;
					prev = current;
					current = next;
				}
				stack.head = prev;
				printStack(stack);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @param stack this function iterates the entire stack
	 */
	public void iterateStack(StackImplementation stack) {
		StackIterator stackIterator = new StackIterator(stack);
		try {

			// if the stack is empty
			if (stackIterator.hasNext() == false) {
				throw new UserDefinedException(Constants.EMPTYSTACK);
			}
			System.out.println(STACKELEMENTS);
			while (stackIterator.hasNext()) {
				System.out.println(stackIterator.next());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
}
