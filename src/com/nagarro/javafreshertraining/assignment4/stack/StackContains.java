package com.nagarro.javafreshertraining.assignment4.stack;

import java.util.Scanner;

import com.nagarro.javafreshertraining.assignment4.exception.UserDefinedException;
import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class StackContains {
	/**
	 * @param stack the function checks weather the element given in input is
	 *              present in the stack or not
	 */
	public void isContain(StackImplementation stack) {
		int count = 0, flag = 0;
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		try {
			// if stack is empty
			if (stack.head == null) {
				throw new UserDefinedException(Constants.EMPTYSTACK);
			} else {
				System.out.println(Constants.ELEMENTSEARCH);
				int element = sc.nextInt();
				StackNode currNode = stack.head;
				while (currNode != null) {
					count++;
					if (currNode.data == element) {

						System.out.println(Constants.ELEMENTPOSITION + count);
						flag = 1;
						break;
					}
					currNode = currNode.next;

				}
				if (flag == 0) {
					throw new UserDefinedException(Constants.NOTFOUND);
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}