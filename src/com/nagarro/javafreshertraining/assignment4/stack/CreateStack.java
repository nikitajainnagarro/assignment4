package com.nagarro.javafreshertraining.assignment4.stack;

import java.util.Scanner;

import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class CreateStack {
	Scanner scanner = new Scanner(System.in);
	private int value;

	public void push(StackImplementation stack) {
		System.out.println(Constants.VALUEINSERT);
		value = scanner.nextInt();
		StackNode new_node = new StackNode(value);
		new_node.next = null;

		if (stack.head == null) {
			new_node.data = value;
			new_node.next = null;
			stack.head = new_node;
		} else {
			new_node.data = value;
			new_node.next = stack.head;
			stack.head = new_node;
		}
		stack.stackSize+=1;
		System.out.println(Constants.INSERTED);
	}
}
