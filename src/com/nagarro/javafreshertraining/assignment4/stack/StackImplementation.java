package com.nagarro.javafreshertraining.assignment4.stack;

import java.util.Scanner;

import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class StackImplementation {
	private static final String PUSH = "1.Push";
	private static final String POP = "2.Pop";
	protected StackNode head;
	protected int stackSize=0;
	public void stackImplementation() {
		StackImplementation stack = new StackImplementation();
		CreateStack create = new CreateStack();
		DeleteStack delete = new DeleteStack();
		ReadStack read = new ReadStack();
		StackContains contains = new StackContains();
		@SuppressWarnings({ "resource" })
		Scanner scanner = new Scanner(System.in);
		String choice = null;	
		while (choice != "10") {

			printInfo();
			choice = scanner.nextLine();
			switch (choice) {
			case "1":
				create.push(stack);
				break;
			case "2":
				delete.pop(stack);
				break;
			case "3":
				read.peek(stack);
				break;
			case "4":
				contains.isContain(stack);
				break;
			case "5":
				read.sizeOfStack(stack);
				break;
			case "6":
				read.reverse(stack);
				break;
			case "7":
				read.iterateStack(stack);
				break;
			case "8":
				read.printStack(stack);
				break;
			case "9":
				return;
			case "10":
			    System.out.println(Constants.EXIT);
				System.exit(0);
				break;
			default:
				System.out.println(Constants.DEFAULT);
			}

		}
	}

	private void printInfo() {
		System.out.println("Choose one of the following stack operations-");
		System.out.println(PUSH);
		System.out.println(POP);
		System.out.println(Constants.THREE+Constants.PEEK);
		System.out.println(Constants.FOUR + Constants.CONTAINS);
		System.out.println(Constants.FIVE + Constants.SIZE);
		System.out.println(Constants.SIX + Constants.REVERSE);
		System.out.println(Constants.SEVEN + Constants.ITERATOR);
		System.out.println(Constants.EIGHT + Constants.TRAVERSE);
		System.out.println(Constants.NINE + Constants.RETURN);
		System.out.println(Constants.TEN + "EXIT");
		System.out.println(Constants.CHOICE);
	}
}
