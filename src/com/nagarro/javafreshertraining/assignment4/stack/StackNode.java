package com.nagarro.javafreshertraining.assignment4.stack;

public class StackNode {
	int data;
	StackNode next;

	/**
	 * @param d
	 */
	StackNode(int d) {
		data = d;
	}
}
