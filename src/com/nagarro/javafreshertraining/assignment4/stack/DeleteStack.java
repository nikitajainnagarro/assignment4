package com.nagarro.javafreshertraining.assignment4.stack;

import com.nagarro.javafreshertraining.assignment4.exception.UserDefinedException;
import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class DeleteStack {
	protected int item;
	protected StackNode temp;

	/**
	 * @param stack his function is used to delete the node from the stack
	 */
	public void pop(StackImplementation stack) {

		{
			try {

				// if the stack is empty
				if (stack.head == null) {
					throw new UserDefinedException(Constants.UNDERFLOW);
				} else {
					item = stack.head.data;
					temp = stack.head;
					stack.head = stack.head.next;
					stack.stackSize-=1;
					System.out.println(Constants.DELETED);
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

}
