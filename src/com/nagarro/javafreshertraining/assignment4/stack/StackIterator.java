package com.nagarro.javafreshertraining.assignment4.stack;

import java.util.Iterator;

public class StackIterator implements Iterator<Integer> {
	private StackNode current;

	/**
	 * @param stack the function is used to iterate the entire stack
	 */
	public StackIterator(StackImplementation stack) {
		current = stack.head;
	}

	public boolean hasNext() {
		return current != null;
	}

	public Integer next() {
		int data = current.data;
		current = current.next;
		return data;
	}
}
