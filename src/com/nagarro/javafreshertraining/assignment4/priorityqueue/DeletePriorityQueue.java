package com.nagarro.javafreshertraining.assignment4.priorityqueue;

import com.nagarro.javafreshertraining.assignment4.exception.UserDefinedException;
import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class DeletePriorityQueue {
	/**
	 * @param priorityQueue 
	 * function delete the element from the priority queue
	 */
	public void deQueue(PriorityQueueImplementation priorityQueue) {
		try {
			// if priority queue is empty
			if (priorityQueue.head == null) {
				throw new UserDefinedException(Constants.UNDERFLOW);
			}
			priorityQueue.head = priorityQueue.head.next;
			System.out.println(Constants.DELETED);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
