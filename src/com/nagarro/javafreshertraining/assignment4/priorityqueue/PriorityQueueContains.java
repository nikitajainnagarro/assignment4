package com.nagarro.javafreshertraining.assignment4.priorityqueue;

import java.util.Scanner;

import com.nagarro.javafreshertraining.assignment4.exception.UserDefinedException;
import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class PriorityQueueContains {

	/**
	 * @param priorityQueue 
	 * function checks weather the given element is present in
	 * the priority queue or not
	 */
	public void isContain(PriorityQueueImplementation priorityQueue) {

		int count = 0, flag = 0;
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		try {
			// if priority queue is empty
			if (priorityQueue.head == null) {
				throw new UserDefinedException(Constants.EMPTYPRIORITYQUEUE);
			}
			PriorityQueueNode currNode = priorityQueue.head;
			System.out.println(Constants.ELEMENTSEARCH);
			int element = sc.nextInt();
			while (currNode != null) {
				count++;
				if (currNode.data == element) {

					System.out.println(Constants.ELEMENTPOSITION + count);
					flag = 1;
					break;
				}
				currNode = currNode.next;

			}
			if (flag == 0) {
				throw new UserDefinedException(Constants.NOTFOUND);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
