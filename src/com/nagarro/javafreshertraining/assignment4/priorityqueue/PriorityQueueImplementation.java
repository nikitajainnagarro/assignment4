package com.nagarro.javafreshertraining.assignment4.priorityqueue;

import java.util.Scanner;

import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class PriorityQueueImplementation {
	protected PriorityQueueNode head;

	/**
	 * @param function is used to play with all the functionalities of priority
	 *                 queue
	 */
	public void priorityQueueImplementation() {

		PriorityQueueImplementation priorityQueue = new PriorityQueueImplementation();
		CreatePriorityQueue create = new CreatePriorityQueue();
		ReadPriorityQueue read = new ReadPriorityQueue();
		DeletePriorityQueue delete = new DeletePriorityQueue();
		PriorityQueueContains contains = new PriorityQueueContains();

		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);

		String choice = null;
		while (choice != "10") {

			printInfo();
			choice = scanner.nextLine();
			switch (choice) {
			case "1":
				create.enqueue(priorityQueue);
				break;
			case "2":
				delete.deQueue(priorityQueue);
				break;
			case "3":
				read.peek(priorityQueue);
				break;
			case "4":
				contains.isContain(priorityQueue);
				break;
			case "5":
				read.sizeOfPriorityQueue(priorityQueue);
				break;
			case "6":
				read.reverse(priorityQueue);
				break;
			case "7":
				read.iteratePriorityQueue(priorityQueue);
				break;
			case "8":
				read.printPriorityQueue(priorityQueue);
				break;
			case "9":
				return;
			case "10":
			    System.out.println(Constants.EXIT);
				System.exit(0);
				break;
			default:
				System.out.println(Constants.DEFAULT);
			}

		}
	}

	private void printInfo() {
		System.out.println("Choose one of the following priority queue operations-");
		System.out.println(Constants.ONE + Constants.ENQUEUE);
		System.out.println(Constants.TWO + Constants.DEQUEUE);
		System.out.println(Constants.THREE + Constants.PEEK);
		System.out.println(Constants.FOUR + Constants.CONTAINS);
		System.out.println(Constants.FIVE + Constants.SIZE);
		System.out.println(Constants.SIX + Constants.REVERSE);
		System.out.println(Constants.SEVEN + Constants.ITERATOR);
		System.out.println(Constants.EIGHT + Constants.TRAVERSE);
		System.out.println(Constants.NINE + Constants.RETURN);
		System.out.println(Constants.TEN + "EXIT");
		System.out.println(Constants.CHOICE);
	}
}
