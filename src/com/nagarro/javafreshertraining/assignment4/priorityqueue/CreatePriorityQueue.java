package com.nagarro.javafreshertraining.assignment4.priorityqueue;

import java.util.Scanner;

import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class CreatePriorityQueue {
	Scanner scanner = new Scanner(System.in);
	private int value, priority;

	/**
	 * @param priorityQueue function inserts the new element to the priority queue
	 */
	public void enqueue(PriorityQueueImplementation priorityQueue) {
		System.out.println(Constants.VALUEINSERT);
		value = scanner.nextInt();
		System.out.println(Constants.PRIORITY);
		priority = scanner.nextInt();

		PriorityQueueNode new_node = new PriorityQueueNode(value, priority);
		new_node.next = null;
		PriorityQueueNode temp = priorityQueue.head;
		if (priorityQueue.head == null || priorityQueue.head.priority > priority) {
			new_node.next = priorityQueue.head;
			priorityQueue.head = new_node;
			System.out.println(Constants.INSERTED);
		}

		else {
			while ((temp.next != null) && ((temp.next.priority) <= priority))
				temp = temp.next;
			new_node.next = temp.next;
			temp.next = new_node;
			System.out.println(Constants.INSERTED);

		}
	}
}
