package com.nagarro.javafreshertraining.assignment4.priorityqueue;

public class PriorityQueueNode {
	int data;
	// Lower values indicate higher priority
	int priority;
	PriorityQueueNode next;

	/**
	 * @param data
	 * @param priority
	 */
	PriorityQueueNode(int data, int priority) {
		this.data = data;
		this.priority = priority;
		next = null;
	}
}
