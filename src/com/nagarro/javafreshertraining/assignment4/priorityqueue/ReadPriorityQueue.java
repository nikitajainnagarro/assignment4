package com.nagarro.javafreshertraining.assignment4.priorityqueue;

import com.nagarro.javafreshertraining.assignment4.exception.UserDefinedException;
import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class ReadPriorityQueue {

	private static final String PRIORITY_QUEUE_ELEMENTS = "Elements of priorityQueue are:";
	private static final String PRIOTITY_QUEUE_SIZE = "Size of priorityQueue is: ";
	private static final String PRIORITY = " Priority = ";
	private static final String DATA = "Data = ";

	/**
	 * @param priorityQueue
	 * function prints the first element of the priority queue
	 *
	 */
	public void peek(PriorityQueueImplementation priorityQueue) {
		try {
			if (priorityQueue.head == null) {
				throw new UserDefinedException(Constants.EMPTYPRIORITYQUEUE);
			}
			System.out.println(DATA + priorityQueue.head.data + PRIORITY + priorityQueue.head.priority);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @param priorityQueue
	 *function prints the entire priority queue
	 */
	public void printPriorityQueue(PriorityQueueImplementation priorityQueue) {
		try {
			if (priorityQueue.head == null) {
				throw new UserDefinedException(Constants.EMPTYPRIORITYQUEUE);
			}
			PriorityQueueNode temp = priorityQueue.head;
			while (temp != null) {
				System.out.println(DATA + temp.data + PRIORITY + temp.priority);

				temp = temp.next;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @param priorityQueue
	 *function prints the size of the priority queue
	 */
	public void sizeOfPriorityQueue(PriorityQueueImplementation priorityQueue) {
		try {
			if (priorityQueue.head == null) {
				throw new UserDefinedException(Constants.EMPTYPRIORITYQUEUE);
			}
			int count = 0;
			PriorityQueueNode currNode = priorityQueue.head;
			while (currNode != null) {
				currNode = currNode.next;
				count++;
			}
			System.out.println(PRIOTITY_QUEUE_SIZE + count);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @param priorityQueue
	 * function prints the reverse of the priority queue
	 *
	 */
	public void reverse(PriorityQueueImplementation priorityQueue) {
		try {
			if (priorityQueue.head == null) {
				throw new UserDefinedException(Constants.EMPTYPRIORITYQUEUE);
			}

			PriorityQueueNode current = priorityQueue.head;
			getSize(current);
			int tempQueue1[] = new int[getSize(current) + 1];
			int iterator = 0;
			current = priorityQueue.head;
			int tempQueue2[] = new int[getSize(current) + 1];
			while (current != null) {
				iterator++;
				tempQueue1[iterator] = current.data;
				tempQueue2[iterator] = current.priority;
				current = current.next;

			}
			for (iterator = getSize(current); iterator > 0; iterator--)
				System.out.println(DATA + tempQueue1[iterator] + PRIORITY + tempQueue2[iterator]);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @param current
	 *function return the size of the priority queue
	 */
	private int getSize(PriorityQueueNode current) {
		int count = 0;
		while (current != null) {
			count++;
			current = current.next;
		}
		return count;
	}

	/**
	 * @param priorityQueue
	 *function iterates over the entire priority queue
	 */
	public void iteratePriorityQueue(PriorityQueueImplementation priorityQueue) {
		PriorityQueueIterator priorityQueueIterator = new PriorityQueueIterator(priorityQueue);
		try {
			if (priorityQueueIterator.hasNext() == false) {
				throw new UserDefinedException(Constants.EMPTYPRIORITYQUEUE);
			}
			System.out.println(PRIORITY_QUEUE_ELEMENTS);
			while (priorityQueueIterator.hasNext()) {
				System.out.println(priorityQueueIterator.next());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
