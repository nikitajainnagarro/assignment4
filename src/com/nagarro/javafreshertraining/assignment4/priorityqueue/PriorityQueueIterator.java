package com.nagarro.javafreshertraining.assignment4.priorityqueue;

import java.util.Iterator;


public class PriorityQueueIterator implements Iterator<Integer> {
	private PriorityQueueNode current;
	/**
	 * @param priorityQueue
	 * function iterates over the entire priority queue
	 */
	public PriorityQueueIterator(PriorityQueueImplementation priorityQueue)
	{
	current= priorityQueue.head;
	}
	public boolean hasNext()
	{
		return current!= null;
	}
	public Integer next()
	{
	int data = current.data;
	current = current.next;
	return data;
	}

}
