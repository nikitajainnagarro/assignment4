package com.nagarro.javafreshertraining.assignment4.queue;

import java.util.Scanner;

import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class QueueImplementation {
	protected QueueNode front;
	protected QueueNode rear;

	/**
	 * @param this function is used to play with all the functionalities of the
	 *             queue
	 */
	public void queueImplementation() {
		QueueImplementation queue = new QueueImplementation();
		CreateQueue create = new CreateQueue();
		DeleteQueue delete = new DeleteQueue();
		ReadQueue read = new ReadQueue();
		QueueContains contains = new QueueContains();
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);

		String choice = null;
		while (choice != "10") {
			printInfo();
			choice = sc.nextLine();
			switch (choice) {
			case "1":
				create.enqueue(queue);
				break;
			case "2":
				delete.dequeue(queue);
				break;
			case "3":
				read.peek(queue);
				break;
			case "4":
				contains.isContain(queue);
				break;
			case "5":
				read.sizeOfQueue(queue);
				break;
			case "6":
				read.reverse(queue);
				break;
			case "7":
				read.iterateQueue(queue);
				break;
			case "8":
				read.printQueue(queue);
				break;
			case "9":
				return;
			case "10":
			    System.out.println(Constants.EXIT);
				System.exit(0);
				break;
			default:
				System.out.println(Constants.DEFAULT);
			}

		}
	}

	private void printInfo() {
		System.out.println("Choose one of the following queue operations-");
		System.out.println(Constants.ONE + Constants.ENQUEUE);
		System.out.println(Constants.TWO + Constants.DEQUEUE);
		System.out.println(Constants.THREE + Constants.PEEK);
		System.out.println(Constants.FOUR + Constants.CONTAINS);
		System.out.println(Constants.FIVE + Constants.SIZE);
		System.out.println(Constants.SIX + Constants.REVERSE);
		System.out.println(Constants.SEVEN + Constants.ITERATOR);
		System.out.println(Constants.EIGHT + Constants.TRAVERSE);
		System.out.println(Constants.NINE + Constants.RETURN);
		System.out.println(Constants.TEN + "EXIT");
		System.out.println(Constants.CHOICE);
	}

}
