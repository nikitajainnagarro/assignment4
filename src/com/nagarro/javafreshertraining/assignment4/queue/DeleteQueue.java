package com.nagarro.javafreshertraining.assignment4.queue;

import com.nagarro.javafreshertraining.assignment4.exception.UserDefinedException;
import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class DeleteQueue {
	protected QueueNode temp;

	/**
	 * @param queue 
	 * function deleted the element from the queue
	 */
	public void dequeue(QueueImplementation queue) {
		try {
			// if the queue is empty
			if (queue.front == null) {
				throw new UserDefinedException(Constants.UNDERFLOW);
			}
			temp = queue.front;
			queue.front = queue.front.next;
			System.out.println(Constants.DELETED);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
