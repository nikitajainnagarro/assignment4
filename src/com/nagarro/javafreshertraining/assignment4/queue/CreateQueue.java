package com.nagarro.javafreshertraining.assignment4.queue;

import java.util.Scanner;

import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class CreateQueue {
	Scanner scanner = new Scanner(System.in);
	private int value;

	/**
	 * @param queue 
	 * function adds new element to the queue
	 */
	public void enqueue(QueueImplementation queue) {
		System.out.println(Constants.VALUEINSERT);
		value = scanner.nextInt();
		QueueNode new_node = new QueueNode(value);
		new_node.next = null;
		new_node.data = value;
		if (queue.front == null) {
			queue.front = new_node;
			queue.rear = new_node;
			queue.front.next = null;
			queue.rear.next = null;
		} else {
			queue.rear.next = new_node;
			queue.rear = new_node;
			queue.rear.next = null;
		}
		System.out.println(Constants.INSERTED);
	}

}
