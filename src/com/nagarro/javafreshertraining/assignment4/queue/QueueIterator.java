package com.nagarro.javafreshertraining.assignment4.queue;

import java.util.Iterator;

public class QueueIterator implements Iterator<Integer> {
	private QueueNode current;

	/**
	 * @param queue used to iterate over the stack
	 */
	public QueueIterator(QueueImplementation queue) {
		current = queue.front;
	}

	/**
	 * @param function checks weather the stack has next element or not
	 */
	public boolean hasNext() {
		return current != null;
	}

	/**
	 * @param return the next element in the stack
	 */
	public Integer next() {
		int data = current.data;
		current = current.next;
		return data;
	}

}
