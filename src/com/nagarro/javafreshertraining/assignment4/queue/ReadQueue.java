package com.nagarro.javafreshertraining.assignment4.queue;

import com.nagarro.javafreshertraining.assignment4.exception.UserDefinedException;
import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class ReadQueue {

	private static final String QUEUESIZE = "Size of queue is: ";
	private static final String QUEUEELEMENTS = "Elements of queue are: ";

	/**
	 * @param queue 
	 * function prints the elements of the queue
	 */
	public void printQueue(QueueImplementation queue) {
		try {
			// if the queue is empty
			if (queue.front == null) {
				throw new UserDefinedException(Constants.EMPTYQUEUE);
			}

			QueueNode currNode = queue.front;
			System.out.println(QUEUEELEMENTS);
			while (currNode != null) {
				System.out.println(currNode.data + " ");
				currNode = currNode.next;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @param queue 
	 * function prints the first element of the queue
	 */
	public void peek(QueueImplementation queue) {
		try {

			// if the queue is empty
			if (queue.front == null) {
				throw new UserDefinedException(Constants.EMPTYQUEUE);
			}
			System.out.println(Constants.PEEKIS + queue.front.data);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @param queue 
	 * function prints the size of the queue
	 */
	public void sizeOfQueue(QueueImplementation queue) {
		try {

			// if the queue is empty
			if (queue.front == null) {
				throw new UserDefinedException(Constants.EMPTYQUEUE);
			}
			int count = 0;
			QueueNode currNode = queue.front;
			while (currNode != null) {
				currNode = currNode.next;
				count++;
			}
			System.out.println(QUEUESIZE + count);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @param queue 
	 * function is used to reverse the queue
	 */
	public void reverse(QueueImplementation queue) {
		try {

			// if the queue is empty
			if (queue.front == null) {
				throw new UserDefinedException(Constants.EMPTYQUEUE);
			}

			QueueNode current = queue.front;
			QueueNode prev = null;
			QueueNode next = null;
			while (current != null) {
				next = current.next;
				current.next = prev;
				prev = current;
				current = next;
			}
			queue.front = prev;
			printQueue(queue);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @param queue 
	 * function iterates over the entire queue
	 */
	public void iterateQueue(QueueImplementation queue) {
		QueueIterator queueIterator = new QueueIterator(queue);
		try {
			if (queueIterator.hasNext() == false) {
				throw new UserDefinedException(Constants.EMPTYQUEUE);
			}
			System.out.println(QUEUEELEMENTS);
			while (queueIterator.hasNext()) {
				System.out.println(queueIterator.next());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}