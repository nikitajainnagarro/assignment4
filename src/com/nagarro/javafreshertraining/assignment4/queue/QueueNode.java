package com.nagarro.javafreshertraining.assignment4.queue;

public class QueueNode {
	int data;
	QueueNode next;

	/**
	 * @param d
	 */
	QueueNode(int d) {
		data = d;
	}
}
