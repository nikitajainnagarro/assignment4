package com.nagarro.javafreshertraining.assignment4.queue;

import com.nagarro.javafreshertraining.assignment4.exception.UserDefinedException;
import com.nagarro.javafreshertraining.assignment4.util.Constants;

import java.util.Scanner;

public class QueueContains {
	/**
	 * @param queue check weather the queue contains the particular element or not
	 */
	public void isContain(QueueImplementation queue) {

		int count = 0, flag = 0;
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		try {
			if (queue.front == null) {
				throw new UserDefinedException(Constants.EMPTYQUEUE);
			}
			QueueNode currNode = queue.front;
			System.out.println(Constants.ELEMENTSEARCH);
			int element = sc.nextInt();
			while (currNode != null) {
				count++;
				if (currNode.data == element) {

					System.out.println(Constants.ELEMENTPOSITION + count);
					flag = 1;
					break;
				}
				currNode = currNode.next;

			}
			if (flag == 0) {
				throw new UserDefinedException(Constants.NOTFOUND);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
