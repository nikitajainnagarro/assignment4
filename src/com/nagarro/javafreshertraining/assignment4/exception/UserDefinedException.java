package com.nagarro.javafreshertraining.assignment4.exception;

public class UserDefinedException extends Exception{
	
		private static final long serialVersionUID = 1L;

		public UserDefinedException(String text) {
			super(text);
		}
}
