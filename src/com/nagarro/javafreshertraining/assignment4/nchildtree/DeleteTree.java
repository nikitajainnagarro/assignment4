package com.nagarro.javafreshertraining.assignment4.nchildtree;

import com.nagarro.javafreshertraining.assignment4.exception.UserDefinedException;
import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class DeleteTree {

	private static final String DELETED = " deleted from the tree";

	/**
	 * @param nchild
	 * @param value
	 * function delete the node from the tree
	 */
	public void deleteNode(NChildTreeImplementation nchild, int value) {
		try
		{
		if (search(nchild, value) == false)
			throw new UserDefinedException(Constants.VALUENOTPRESENT);
		else {
			nchild.root = deleteNode(nchild.root, value);
			System.out.println(value + DELETED);
		}
		}catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @param nchild
	 * @param val
	 * @return
	 */
	public boolean search(NChildTreeImplementation nchild, int val) {
		return search(nchild.root, val);
	}

	
	/**
	 * @param r
	 * @param val
	 * @return
	 *Function to search for an element recursively
	 */
	private boolean search(TreeNode r, int val) {
		boolean found = false;
		while ((r != null) && !found) {
			int rval = r.data;
			if (val < rval)
				r = r.left;
			else if (val > rval)
				r = r.right;
			else {
				found = true;
				break;
			}
			found = search(r, val);
		}
		return found;
	}

	/**
	 * @param root
	 * @param value
	 * @return
	 */
	private TreeNode deleteNode(TreeNode root, int value) {
		TreeNode p, p2, n;
		if (root.data == value) {
			TreeNode lt, rt;
			lt = root.left;
			rt = root.right;
			if (lt == null && rt == null)
				return null;
			else if (lt == null) {
				p = rt;
				return p;
			} else if (rt == null) {
				p = lt;
				return p;
			} else {
				p2 = rt;
				p = rt;
				while (p.left != null)
					p = p.right;
				p.left = (lt);
				return p2;
			}
		}
		if (value < root.data) {
			n = deleteNode(root.left, value);
			root.left = n;
		} else {
			n = deleteNode(root.right, value);
			root.right = (n);
		}
		return root;
	}
}
