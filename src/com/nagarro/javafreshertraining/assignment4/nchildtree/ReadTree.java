package com.nagarro.javafreshertraining.assignment4.nchildtree;

import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class ReadTree {

	private static final String ROOTNODE = "The node is the root node";
	private static final String VALUE_LEFTNODE = "The given value is the left node of the nodewith value ";
	private static final String VALUE_RIGHTNODE = "The given value is the right node of the node with value ";

	/**
	 * @param nChildTree prints the elements of the tree in level order traversal
	 */
	public void printBreadthFirst(NChildTreeImplementation nChildTree) {
		int h = height(nChildTree.root);
		int i;
		for (i = 1; i <= h; i++)
			printGivenLevel(nChildTree.root, i);
		System.out.println("");
	}

	/**
	 * @param root
	 * @return Compute the "height" of a tree -- the number of nodes along the
	 *         longest path from the root node down to the farthest leaf node.
	 */
	int height(TreeNode root) {
		if (root == null)
			return 0;
		else {
			/* compute height of each subtree */
			int lheight = height(root.left);
			int rheight = height(root.right);

			/* use the larger one */
			if (lheight > rheight)
				return (lheight + 1);
			else
				return (rheight + 1);
		}
	}

	/**
	 * @param root
	 * @param level Print nodes at the given level
	 */
	void printGivenLevel(TreeNode root, int level) {
		if (root == null)
			return;
		if (level == 1)
			System.out.print(root.data + " ");
		else if (level > 1) {
			printGivenLevel(root.left, level - 1);
			printGivenLevel(root.right, level - 1);
		}
	}

	/**
	 * @param node prints the post order traversal of the tree
	 */
	void printPostorder(TreeNode node) {
		if (node == null)
			return;

		// first recur on left subtree
		printPostorder(node.left);

		// then recur on right subtree
		printPostorder(node.right);

		// now deal with the node
		System.out.print(node.data + " ");
	}

	/**
	 * @param node prints the in order traversal of the tree
	 */
	void printInorder(TreeNode node) {
		if (node == null)
			return;

		/* first recur on left child */
		printInorder(node.left);

		/* then print the data of node */
		System.out.print(node.data + " ");

		/* now recur on right child */
		printInorder(node.right);
	}

	/**
	 * @param node prints the pre order traversal of the tree
	 */
	void printPreorder(TreeNode node) {
		if (node == null)
			return;

		/* first print data of node */
		System.out.print(node.data + " ");

		/* then recur on left subtree */
		printPreorder(node.left);

		/* now recur on right subtree */
		printPreorder(node.right);
	}

	/**
	 * @param root
	 * @param value
	 */
	public void getElementByValue(TreeNode root, int value) {
		TreeNode curr = root;
		TreeNode parent = null;
		while (curr != null && curr.data != value) {
			parent = curr;
			if (value < curr.data) {
				curr = curr.left;

			} else {
				curr = curr.right;
			}
		}
		if (curr == null) {
			System.out.println(Constants.VALUENOTPRESENT);
			return;
		}
		if (parent == null) {
			System.out.println(ROOTNODE);
		} else if (value < parent.data) {
			System.out.println(VALUE_LEFTNODE + parent.data);

		} else {
			System.out.println(VALUE_RIGHTNODE + parent.data);
		}
	}
}
