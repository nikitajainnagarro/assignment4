package com.nagarro.javafreshertraining.assignment4.nchildtree;

import java.util.Scanner;

import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class NChildTreeImplementation {
	private static final String ELEMENT_NOTPRESENT = "Element is not present in the tree";
	private static final String ELEMENT_PRESENT = "Element is present in the tree";
	private static final String DFS_TYPE = "Choose the type of DFS\n1.Inorder \n2.Preoder\n3.PostOrder";
	private static final String TRAVERSE_DFS = "Traverse/Print Depth First";
	private static final String TRAVERSE_BFS = "Traverse/Print Breadth First";
	private static final String ITERATOR_DFS = "Iterator Depth First";
	private static final String ITERATOR_BFS = "Iterator Breadth First";
	private static final String GET_BY_LEVEL = "Get Elements by level";
	private static final String GET_BY_VALUE = "Get Elements by value";
	public TreeNode root;  
    
    public NChildTreeImplementation(){  
      root = null;  
    }  
      
	public void nChildTreeImplementation()
	{
	NChildTreeImplementation nChildTree = new NChildTreeImplementation();
	CreateTree create = new CreateTree();
	ReadTree read = new ReadTree();
	TreeContains contains = new TreeContains();
	DeleteTree delete = new DeleteTree();
	LevelTraversal levelTraversal = new LevelTraversal();
	@SuppressWarnings("resource")
	Scanner scanner = new Scanner(System.in);
	int value;
    String choice =null;  
    int depthTraverseChoice;
    while(choice != "11")   
    {  
    	
        
        printInfo();         
        choice = scanner.nextLine();
        switch(choice)  
        {  
            case "1":  
            	System.out.println(Constants.VALUEINSERT);
            	value = scanner.nextInt();
            	create.insert(value, nChildTree);
            	break;  
            case "2":  
            	System.out.println(Constants.VALUEDELETE);
            	value = scanner.nextInt();
            	delete.deleteNode(nChildTree,value);
            	break;  
            case "3":  
            	System.out.println(Constants.ELEMENTSEARCH);
            	value = scanner.nextInt();
            	contains.containsNode(nChildTree.root, value);
            	if(contains.flag==true)
            	System.out.println(ELEMENT_PRESENT);  
                else  
                System.out.println(ELEMENT_NOTPRESENT); 
            	break;  
            case "4":  
            	System.out.println(Constants.ELEMENTSEARCH);
            	value = scanner.nextInt();
            	read.getElementByValue(nChildTree.root, value);       
            	break;  
            case "5":  
            	levelTraversal.printByLevel(nChildTree.root);
            	break;  
            case "6":  
            	System.out.println(ITERATOR_BFS);
            	read.printBreadthFirst(nChildTree);
            	break;  
            case "7":  
            	System.out.println(ITERATOR_DFS);
            	System.out.println(DFS_TYPE);
            	depthTraverseChoice = scanner.nextInt();
            	depthTraverseInfo(nChildTree, read, depthTraverseChoice);
            	break;  
            case "8":  
            	System.out.println(TRAVERSE_BFS);
            	read.printBreadthFirst(nChildTree);
            	break;
            case "9":
            	System.out.println(DFS_TYPE);
            	depthTraverseChoice = scanner.nextInt();
			depthTraverseInfo(nChildTree, read, depthTraverseChoice);
            	break;
            case "10":  
            	return;
            case "11":
                System.out.println(Constants.EXIT);
            	System.exit(0);
            	break;
            default:  
            	System.out.println(Constants.DEFAULT);  
        }  

}
	}

	private void printInfo() {
		System.out.println(Constants.CHOICE);  
        System.out.println(Constants.INSERT);
        System.out.println(Constants.DELETE);
        System.out.println(Constants.THREE+Constants.CONTAINS);
        System.out.println(Constants.FOUR+GET_BY_VALUE);
        System.out.println(Constants.FIVE+GET_BY_LEVEL);
        System.out.println(Constants.SIX+ITERATOR_BFS);
        System.out.println(Constants.SEVEN+ITERATOR_DFS);
        System.out.println(Constants.EIGHT+TRAVERSE_BFS);
        System.out.println(Constants.NINE+TRAVERSE_DFS);
        System.out.println(Constants.TEN+Constants.RETURN);
        System.out.println(Constants.ELEVEN+"EXIT");
        System.out.println(Constants.CHOICE);
	}

	private void depthTraverseInfo(NChildTreeImplementation nChildTree, ReadTree read, int depthTraverseChoice) {
		switch(depthTraverseChoice) {
			case 1:;
				read.printInorder(nChildTree.root);
				System.out.println("");
				break;
			case 2:
				read.printPreorder(nChildTree.root);
				System.out.println("");
				break;
			case 3:
				read.printPostorder(nChildTree.root);
				System.out.println("");
				break;
			default:
				System.out.println(Constants.DEFAULT);
				System.out.println("");
		}
	}

}
