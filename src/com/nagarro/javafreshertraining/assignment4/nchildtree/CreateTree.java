package com.nagarro.javafreshertraining.assignment4.nchildtree;

public class CreateTree {
	/**
	 * @param value
	 * @param nChildTree function inserts the given value into the tree
	 */
	public void insert(int value, NChildTreeImplementation nChildTree) {
		TreeNode newNode = new TreeNode(value);

		// Check whether tree is empty
		if (nChildTree.root == null) {
			nChildTree.root = newNode;
			return;
		} else {
			// current node point to root of the tree
			TreeNode current = nChildTree.root, parent = null;

			while (true) {
				// parent keep track of the parent node of current node.
				parent = current;

				// If data is less than current's data, node will be inserted to the left of
				// tree
				if (value < current.data) {
					current = current.left;
					if (current == null) {
						parent.left = newNode;
						return;
					}
				}
				// If data is greater than current's data, node will be inserted to the right of
				// tree
				else {
					current = current.right;
					if (current == null) {
						parent.right = newNode;
						return;
					}
				}
			}
		}
	}
}
