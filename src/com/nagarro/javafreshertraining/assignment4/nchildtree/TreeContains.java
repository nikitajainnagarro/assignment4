package com.nagarro.javafreshertraining.assignment4.nchildtree;

import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class TreeContains {
	 boolean flag;
	  /**
	 * @param temp
	 * @param value
	 * function checks weather the given value value is present in the tree or not
	 */
	public void containsNode(TreeNode temp, int value){
		  flag = false;
	        //Check whether tree is empty  
	        if(temp == null){  
	          System.out.println(Constants.EMPTYTREE);  
	        }  
	        else{  
	          //If value is found in the given binary tree then, set the flag to true  
	          if(temp.data == value){  
	            flag = true;  
	             return;  
	          }  
	          //Search in left subtree  
	          if(flag == false && temp.left != null){  
	             containsNode(temp.left, value);  
	          }  
	          //Search in right subtree  
	          if(flag == false && temp.right != null){  
	             containsNode(temp.right, value);  
	          }  
	        }  
	       }
}