package com.nagarro.javafreshertraining.assignment4.nchildtree;

public class TreeNode {
	int data;
	TreeNode left;
	TreeNode right;

	/**
	 * @param data
	 */
	public TreeNode(int data) {
		// Assign data to the new node, set left and right children to null
		this.data = data;
		this.left = null;
		this.right = null;
	}

}
