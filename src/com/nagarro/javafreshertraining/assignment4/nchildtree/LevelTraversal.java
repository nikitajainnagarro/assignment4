package com.nagarro.javafreshertraining.assignment4.nchildtree;

import java.util.LinkedList;
import java.util.Queue;

public class LevelTraversal {
	/**
	 * @param root PRINTS THE ELEMENTS OF THE TREE LEVEL WISE
	 */
	void printByLevel(TreeNode root) {
		// Base Case
		if (root == null)
			return;

		// Create an empty queue for level order tarversal
		Queue<TreeNode> q = new LinkedList<TreeNode>();

		// Enqueue Root and initialize height
		q.add(root);

		while (true) {

			// nodeCount (queue size) indicates number of nodes
			// at current level.
			int nodeCount = q.size();
			if (nodeCount == 0)
				break;

			// Dequeue all nodes of current level and Enqueue all
			// nodes of next level
			while (nodeCount > 0) {
				TreeNode node = q.peek();
				System.out.print(node.data + " ");
				q.remove();
				if (node.left != null)
					q.add(node.left);
				if (node.right != null)
					q.add(node.right);
				nodeCount--;
			}
			System.out.println();
		}
	}

}
