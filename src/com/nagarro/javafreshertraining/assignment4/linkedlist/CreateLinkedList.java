package com.nagarro.javafreshertraining.assignment4.linkedlist;

import java.util.Scanner;

import com.nagarro.javafreshertraining.assignment4.exception.UserDefinedException;
import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class CreateLinkedList {
	Scanner scanner = new Scanner(System.in);
	private int value, position;
	ReadLinkedList read = new ReadLinkedList();

	public void insertAtEnd(LinkedListImplementation linkedList) {
		System.out.println(Constants.VALUEINSERT);
		value = scanner.nextInt();
		LinkedListNode new_node = new LinkedListNode(value);
		new_node.next = null;
		// If the Linked List is empty
		// then make the new node as head
		if (linkedList.head == null) {
			linkedList.head = new_node;
		} else {
			// Else traverse till the last node
			// and insert the new_node there
			LinkedListNode last = linkedList.head;
			while (last.next != null) {
				last = last.next;
			}

			// Insert the new_node at last node
			last.next = new_node;
			System.out.println(Constants.INSERTED);
		}

	}

	public void insertAtBeginning(LinkedListImplementation linkedList) {
		// Create a new node with given data
		LinkedListNode new_node = new LinkedListNode(value);
		new_node.next = null;
		System.out.println(Constants.VALUEINSERT);
		value = scanner.nextInt();
		new_node.data = value;
		new_node.next = linkedList.head;
		linkedList.head = new_node;

		System.out.println(Constants.INSERTED);
	}

	public void insertAtPosition(LinkedListImplementation linkedList) {
		System.out.println(Constants.POSINSERT);
		position = scanner.nextInt();
		int size = read.sizeOfLinkedList(linkedList);
		try {
			if (position == size + 1) {
				insertAtEnd(linkedList);
			}

			else if (position == 1) {
				insertAtBeginning(linkedList);
			} else if (size < position) {
				throw new UserDefinedException(Constants.ERRORINSERT);
			} else {
				System.out.println(Constants.VALUEINSERT);
				value = scanner.nextInt();
				int i = 1;
				LinkedListNode temp = linkedList.head;
				// Create a new node with given data
				LinkedListNode new_node = new LinkedListNode(value);
				new_node.next = null;
				while (i < position - 1) {
					temp = temp.next;
					i++;
				}
				new_node.next = temp.next;
				temp.next = new_node;
				System.out.println(Constants.INSERTED);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}