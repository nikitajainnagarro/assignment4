package com.nagarro.javafreshertraining.assignment4.linkedlist;

import com.nagarro.javafreshertraining.assignment4.exception.UserDefinedException;
import com.nagarro.javafreshertraining.assignment4.util.Constants;

import java.util.Scanner;

public class DeleteLinkedList {

	Scanner scanner = new Scanner(System.in);
	private int position;
	ReadLinkedList read = new ReadLinkedList();

	public void deleteFromBeginning(LinkedListImplementation linkedList) {
		try {
			if (linkedList.head == null) {
				throw new UserDefinedException(Constants.EMPTYLIST);
			} else {
				LinkedListNode temp;
				temp = linkedList.head;
				linkedList.head = temp.next;
				System.out.println(Constants.DELETED);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void deleteFromEnd(LinkedListImplementation linkedList) {
		try {
			if (linkedList.head == null) {
				throw new UserDefinedException(Constants.EMPTYLIST);
			} 
			else if (linkedList.head.next == null) {
				linkedList.head = null;
				System.out.println(Constants.DELETED);
			}
			else {
				LinkedListNode temp, temp1 = null;
				temp = linkedList.head;
				while (temp.next != null) {
					temp1 = temp;
					temp = temp.next;
				}
				temp1.next = null;
				System.out.println(Constants.DELETED);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public void deleteFromPosition(LinkedListImplementation linkedList) {
		System.out.println(Constants.POSDELETE);
		position = scanner.nextInt();
		int size = read.sizeOfLinkedList(linkedList);
		try {
			if (position == size) {
				deleteFromEnd(linkedList);
			}

			else if (position == 1) {
				deleteFromBeginning(linkedList);
			} else if (size < position) {
				throw new UserDefinedException(Constants.ERRORDELETE+position+"!Try smaller position.");
			} else {
				int i = 1;
				LinkedListNode temp, temp1 = null;
				temp = linkedList.head;
				while (i < position) {
					temp1 = temp;
					temp = temp.next;
					i++;
				}
				temp1.next = temp.next;
				System.out.println(Constants.DELETED);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}