package com.nagarro.javafreshertraining.assignment4.linkedlist;

public class LinkedListNode {
	int data;
	LinkedListNode next;

	/**
	 * @param data
	 */
	LinkedListNode(int data) {
		this.data = data;
	}
}
