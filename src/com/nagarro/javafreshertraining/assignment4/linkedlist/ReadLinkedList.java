package com.nagarro.javafreshertraining.assignment4.linkedlist;

import com.nagarro.javafreshertraining.assignment4.exception.UserDefinedException;
import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class ReadLinkedList {
	private static final String LISTELEMENTS = "Elements of List are";
	private static final String CENTRE = "Centre of linked list is: ";
	private static final String CENTRE1 = "Centre1 of linked list is: ";
	private static final String CENTRE2 = "Centre2 of linked list is: ";

	public void printList(LinkedListImplementation linkedList) {
		try {
			if (linkedList.head == null) {

				throw new UserDefinedException(Constants.EMPTYLIST);
			} else {
				LinkedListNode currNode = linkedList.head;
				System.out.println(LISTELEMENTS);
				// Traverse through the LinkedList
				while (currNode != null) {
					// Print the data at current node
					System.out.print(currNode.data + " ");
					// Go to next node
					currNode = currNode.next;
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public int sizeOfLinkedList(LinkedListImplementation linkedList) {
		int count = 0;
		LinkedListNode currNode = linkedList.head;
		while (currNode != null) {
			currNode = currNode.next;
			count++;
		}
		return count;

	}

	public void centreOfList(LinkedListImplementation linkedList) {
		int mid;
		LinkedListNode temp = linkedList.head;
		int count = 0;
		int size = sizeOfLinkedList(linkedList);
		try {
			if (size == 0) {
				throw new UserDefinedException(Constants.EMPTYLIST);
			} else if (size == 1) {
				System.out.println(LISTELEMENTS + temp.data);
			} else if (size % 2 != 0) {
				mid = size / 2;

				while (count != mid) {
					temp = temp.next;
					count++;
				}
				System.out.println(CENTRE + temp.data);
			} else if (size % 2 == 0) {
				mid = size / 2;

				while (count != mid - 1) {
					temp = temp.next;
					count++;
				}

				System.out.println(CENTRE1 + temp.data);
				temp = temp.next;
				System.out.println(CENTRE2 + temp.data);

			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void reverse(LinkedListImplementation linkedList) {
		try {
			if (linkedList.head == null) {
				throw new UserDefinedException(Constants.EMPTYLIST);
			} else {
				LinkedListNode current = linkedList.head;
				LinkedListNode prev = null;
				LinkedListNode next = null;
				while (current != null) {
					next = current.next;
					current.next = prev;
					prev = current;
					current = next;
				}
				linkedList.head = prev;
				printList(linkedList);
			}
		}

		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void iterateList(LinkedListImplementation linkedList) {
		ListIterator listIterator = new ListIterator(linkedList);
		try {
			if (listIterator.hasNext() == false) {
				throw new UserDefinedException(Constants.EMPTYLIST);
			}
			System.out.println(LISTELEMENTS);
			while (listIterator.hasNext()) {
				System.out.println(listIterator.next());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
}