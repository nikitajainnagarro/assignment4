package com.nagarro.javafreshertraining.assignment4.linkedlist;

import java.util.Iterator;

public class ListIterator implements Iterator<Integer> {
	private LinkedListNode current;

	/**
	 * @param linkedList
	 * this is the implementation of linked list iterator
	 */
	public ListIterator(LinkedListImplementation linkedList) {
		current = linkedList.head;
	}

	public boolean hasNext() {
		return current != null;
	}

	public Integer next() {
		int data = current.data;
		current = current.next;
		return data;
	}
}
