package com.nagarro.javafreshertraining.assignment4.linkedlist;

import java.util.Scanner;

import com.nagarro.javafreshertraining.assignment4.util.Constants;

public class LinkedListImplementation {
	protected LinkedListNode head;
	private int size;
	private static final String BEGININSERT= "1.Insert at Beginning";
	private static final String ENDINSERT= "2.Insert at End";
	private static final String POSINSERT= "3.Insert at Position";
	private static final String BEGINDELETE= "4.Delete from Beginning";
	private static final String ENDDELETE= "5.Delete from End";
	private static final String POSDELETE= "6.Delete from specified Position";
	private static final String LINKEDLISTSIZE = "Size of a linked list is: ";

	public void linkedListImplementation() {
		LinkedListImplementation linkedList = new LinkedListImplementation();
		CreateLinkedList create = new CreateLinkedList();
		DeleteLinkedList delete = new DeleteLinkedList();
		ReadLinkedList read = new ReadLinkedList();
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		String choice = null;
		while (choice != "13") {
			printInfo();
			choice = scanner.nextLine();
			switch (choice) {
			case "1":
				create.insertAtBeginning(linkedList);
				break;
			case "2":
				create.insertAtEnd(linkedList);
				break;
			case "3":
				create.insertAtPosition(linkedList);
				break;
			case "4":
				delete.deleteFromBeginning(linkedList);
				break;
			case "5":
				delete.deleteFromEnd(linkedList);
				break;
			case "6":
				delete.deleteFromPosition(linkedList);
				break;
			case "7":
				read.centreOfList(linkedList);
				break;
			case "8":
				read.reverse(linkedList);
				System.out.println("");
				break;
			case "9":
				size = read.sizeOfLinkedList(linkedList);
				System.out.println(LINKEDLISTSIZE + size);
				break;
			case "10":
				read.iterateList(linkedList);
				break;
			case "11":
				read.printList(linkedList);
				System.out.println("");
				break;
			case "12":
				return;
			case "13":
			    System.out.println(Constants.EXIT);
				System.exit(0);
				break;
			default:
				System.out.println(Constants.DEFAULT);
			}
		}
	}
	private void printInfo() {
		System.out.println("Choose one of the following Linked List operations-");
		System.out.println(BEGININSERT);
		System.out.println(ENDINSERT);
		System.out.println(POSINSERT);
		System.out.println(BEGINDELETE);
		System.out.println(ENDDELETE);
		System.out.println(POSDELETE);
		System.out.println(Constants.SEVEN+Constants.CENTER);
		System.out.println(Constants.EIGHT+Constants.REVERSE);
		System.out.println(Constants.NINE+Constants.SIZE);
		System.out.println(Constants.TEN+Constants.ITERATOR);
		System.out.println(Constants.ELEVEN+Constants.TRAVERSE);
		System.out.println(Constants.TWELVE+Constants.RETURN);
		System.out.println(Constants.THIRTEEN+"EXIT");
		System.out.println(Constants.CHOICE);
	}
}